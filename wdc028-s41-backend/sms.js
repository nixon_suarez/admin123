const request = require('request')

module.exports.send = (mobileNumber, message) => {
    const shortCode = '21581141'
    const accessToken = 'sfw73G4ndgkz8FYFNtikR49RvjjZa7AuIYyThAq19rk'
    const clientCorrelator = '153789'

    const options = {
        method: 'POST',
        url: 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/' + shortCode + '/requests',
        qs: { 'access_token': accessToken },
        headers: { 'Content-Type': 'application/json' },
        body: {
            'outboundSMSMessageRequest': { 
                'clientCorrelator': clientCorrelator,
                'senderAddress': shortCode,
                'outboundSMSTextMessage': { 'message': message },
                'address': mobileNumber 
            } 
        },
        json: true 
    }

    request(options, (error, response, body) => {
        if (error) throw new Error(error)
        console.log(body)
    })
}